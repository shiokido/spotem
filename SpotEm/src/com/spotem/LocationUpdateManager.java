package com.spotem;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.spotem.data.Contact;

public class LocationUpdateManager implements GooglePlayServicesClient.ConnectionCallbacks,
GooglePlayServicesClient.OnConnectionFailedListener,
LocationListener{
	private static LocationUpdateManager instance = new LocationUpdateManager();
	
	public static LocationUpdateManager instance(){
		return instance;
	}
	
	private Thread locUpdaterThread;
	private LocationUpdater locUpdater;
	private LocationManager locManager;
	public LocationClient locClient;
	
	public void init(Context context){
		if (locClient != null) return;
		
		locClient = new LocationClient(context, this, this);
		locClient.connect();
		
		try{
			TelephonyManager tMgr = (TelephonyManager) context
					.getSystemService(Context.TELEPHONY_SERVICE);
			Contact ownPhone = new Contact(tMgr.getLine1Number(), "You");

			locUpdater = new LocationUpdater(ownPhone.phoneNumber, locClient);
			locUpdaterThread = new Thread(locUpdater);
			locUpdaterThread.start();
		}catch(Exception ex){
			Log.e("SpotEm", "Could not find own phone number");
		}
	}
	
	public void setSharing(boolean isSharing){
		locUpdater.IsSharing = isSharing;
	}

	@Override
	public void onLocationChanged(Location loc) {
	    Log.e("LOC", "Update " + loc.getLatitude());
		
	}

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onConnected(Bundle arg0) {
		 Location locc = locClient.getLastLocation();
		 Log.e("LOC", "FirstPos: " + locc.getLatitude());
	}

	@Override
	public void onDisconnected() {
		// TODO Auto-generated method stub
		
	}
}
