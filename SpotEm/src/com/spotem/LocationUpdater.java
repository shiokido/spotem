package com.spotem;

import com.google.android.gms.location.LocationClient;
import com.spotem.data.Location;
import com.spotem.data.SpotEmWebservice;

public class LocationUpdater implements Runnable {
	public LocationUpdater(String phoneNumber, LocationClient locClient) {
		this.locClient = locClient;
		this.phoneNumber = phoneNumber;
	}

	volatile boolean IsSharing;
	private LocationClient locClient;
	private String phoneNumber;
	private Location location = new Location();
	
	@Override
	public void run() {
		while(true){
			if (IsSharing && locClient.isConnected()){
				android.location.Location loc = locClient.getLastLocation();
				location.setLat(loc.getLatitude());
				location.setLong(loc.getLongitude());
				
				SpotEmWebservice.setLocation(phoneNumber, location);
			}

			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				return; //thread shutdown, leave loop;
			}
		}
	}
}
