package com.spotem;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.widget.Toast;

import com.spotem.data.SpotEmWebservice;

public class ServiceAvailabilityTask extends AsyncTask<ConnectivityManager, Integer, Boolean>{
	public Context context;
	@Override
	protected Boolean doInBackground(ConnectivityManager... params) {
		return SpotEmWebservice.checkOnline(params[0]);
	}
	
	@Override
	 protected void onPostExecute(Boolean available) {
		if (!available){
			Toast toast = Toast.makeText(context, "No Internet connection or webservice not available.", 5);
			toast.show();
		}
	 }
}
