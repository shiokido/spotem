package com.spotem;

import com.spotem.data.Contact;
import com.spotem.data.ContactList;
import com.spotem.data.SpotEmWebservice;
import com.spotem.data.Location;

import android.os.AsyncTask;
import android.widget.TextView;

public class GetLocationTask extends AsyncTask<Contact, Integer, Location>{
	private Contact contact;
	
	@Override
	protected Location doInBackground(Contact... params) {
		contact = params[0];
		return SpotEmWebservice.getLocationFromPhoneNumber(contact.phoneNumber);
	}
	
	@Override
	 protected void onPostExecute(Location location) {
		if (location != null){
			if (ContactList.contacts.containsKey(contact.phoneNumber)){
				Location curLoc = ContactList.contacts.get(contact.phoneNumber).location;
				curLoc.setLat(location.getLat());
				curLoc.setLong(location.getLong());
			}else{
				contact.location = location;
				ContactList.contacts.put(contact.phoneNumber, contact);
			}
		}else{
			if (ContactList.contacts.containsKey(contact.phoneNumber)){
				ContactList.contacts.remove(contact.phoneNumber);
			}
		}
	 }
}
