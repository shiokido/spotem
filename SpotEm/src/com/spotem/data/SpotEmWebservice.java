package com.spotem.data;

import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class SpotEmWebservice {
	private static String IP = "http://128.39.42.67";
	private static String SET_URL = IP + "/spotem/location/set/";
	private static String GET_URL = IP + "/spotem/location/get/";

	public static Location getLocationFromPhoneNumber(String phoneNumber) {
		String uri = GET_URL;
		uri += phoneNumber;

		HttpClient httpClient = new DefaultHttpClient();
		HttpGet get = new HttpGet(uri);
		HttpResponse response;

		Location loc = new Location();
		try {
			response = httpClient.execute(get);
			int statusCode = response.getStatusLine().getStatusCode();
			if (statusCode == 404) {
				return null;
			}

			String result = EntityUtils.toString(response.getEntity());
			String[] split = result.split("\\s+");
			loc.setLat(Double.parseDouble(split[0]));
			loc.setLong(Double.parseDouble(split[1]));
		} catch (Exception e) {
			Log.e("Webservice error", e.toString());
			return null;
		}
		return loc;
	}

	public static boolean checkOnline(ConnectivityManager cm) {
		try {
			NetworkInfo netInfo = cm.getActiveNetworkInfo();
			if (netInfo != null && netInfo.isConnected()) {
				URL url = new URL(IP + "/spotem");
				HttpURLConnection urlc = (HttpURLConnection) url
						.openConnection();
				urlc.setRequestProperty("Connection", "close");
				urlc.setConnectTimeout(2000);
				urlc.connect();

				//501 = Not implemented  - is expected for the service to be online
				if (urlc.getResponseCode() == 501){ 
					return true;
				} else {
					return false;
				}
			}
		} catch (Exception ex) {
			Log.e("asd", ex.getMessage());
			return false;
		}
		return false;
	}

	public static void setLocation(String phoneNumber, Location loc) {
		String uri = SET_URL;
		uri += phoneNumber + "/";
		uri += "?lat=" + loc.getLat();
		uri += "&long=" + loc.getLong();

		HttpClient httpClient = new DefaultHttpClient();
		HttpGet get = new HttpGet(uri);
		try {
			HttpResponse resp = httpClient.execute(get);
			if (!EntityUtils.toString(resp.getEntity()).equals("success")) {
				Log.e("Webservice", "Could not set location");
			}
		} catch (Exception e) {
			Log.e("Webservice", e.getMessage());
		}
	}
}
