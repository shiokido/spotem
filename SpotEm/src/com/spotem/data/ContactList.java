package com.spotem.data;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.telephony.TelephonyManager;

public class ContactList {
	public static Map<String, Contact> contacts = new HashMap<String, Contact>();
	public static final boolean INCLUDE_TEST_DUMMIES = true;
	public static Contact MovingDummy;
	
	public static void Initalize(Context context) {
		Random rnd = new Random();
		
		if (INCLUDE_TEST_DUMMIES){
			String PhoneNumber = "+4917" + (rnd.nextInt(999999) + 1000);
			Contact gjovikDummy1 = new Contact(PhoneNumber, "GjovikTestStatic");
			gjovikDummy1.location.setLat(60.795578);
			gjovikDummy1.location.setLong(10.691927);
			addItem(gjovikDummy1);
			
			PhoneNumber = "+4917" + (rnd.nextInt(999999) + 1000);
			MovingDummy = new Contact(PhoneNumber, "GjovikTestMoving");
			MovingDummy.location.setLat(60.788945);
			MovingDummy.location.setLong(10.681626);
			addItem(MovingDummy);
		}
	}

	public static void addItem(Contact contact) {
		contacts.put(contact.phoneNumber, contact);
	}
}
