package com.spotem.data;

public class Location{
	private double latitude = -1;
	private double longitude = -1;
	private LocationChangedListener listener;
	
	public boolean isSet(){
		return latitude != -1 && longitude != -1;
	}
	
	public double getLong(){
		return longitude;
	}
	
	public double getLat(){
		return latitude;
	}
	
	public void setLong(double longi){
		longitude = longi;
		onLocationChanged();
	}
	
	public void setLat(double lat){
		latitude = lat;
		onLocationChanged();
	}
	
	private void onLocationChanged(){
		if (listener != null){
			listener.LocationChanged(this);
		}
	}
	public void setListener(LocationChangedListener listener){
		this.listener = listener;
	}
	
	@Override
	public String toString(){
		if (!isSet()){
			return "No location";
		}
		return "La{" + String.format("%1$,.2f}", latitude) + " Lo{" + String.format("%1$,.2f}", longitude);
	}
}