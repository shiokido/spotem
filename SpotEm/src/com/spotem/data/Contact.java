package com.spotem.data;

public class Contact {
	public String phoneNumber;
	public String name;
	public Location location;
	
	public Contact(String phoneNumber, String name) {
		this.phoneNumber = phoneNumber;
		this.name = name;
		location = new Location();
	}

	@Override
	public String toString() {
		if (location != null){
			return name;
		}
		return name;
	}
}