package com.spotem;

import java.util.HashMap;
import java.util.Map;

import android.app.Dialog;
import android.app.DialogFragment;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

import com.assignment.R;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.spotem.data.Contact;
import com.spotem.data.ContactList;
import com.spotem.data.LocationChangedListener;

public class ContactDetailFragment extends Fragment {
	public static final String ARG_ITEM_ID = "item_id";
	private GoogleMap map;
	private HashMap<Contact, Marker> markers = new HashMap<Contact, Marker>();
	private Contact selectedContact;
	private boolean isShowingOwn = false;
	
	public ContactDetailFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (getArguments().containsKey(ARG_ITEM_ID)) {
			selectedContact = ContactList.contacts.get(getArguments().getString(
					ARG_ITEM_ID));
		}
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView;
		if (selectedContact != null){
			getActivity().setTitle(selectedContact.name);
			rootView = inflater.inflate(R.layout.fragment_contact_detail, container, false);
			Button btnShowOwn = (Button)rootView.findViewById(R.id.btnShowOwn);
			btnShowOwn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					isShowingOwn = true;
					zoomToLoc(LocationUpdateManager.instance().locClient.getLastLocation());
				}
			});
			
			Button btnShowTarget = (Button)rootView.findViewById(R.id.btnShowTarget);
			btnShowTarget.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					isShowingOwn = false;
					zoomToLoc(selectedContact.location);
				}
			});
		}else{
			rootView = inflater.inflate(R.layout.fragment_all_contacts, container, false);
		}
		
		InitMap();
		
		markers.clear();
		if (selectedContact != null) {
			CreateMarker(selectedContact);
			zoomToLoc(selectedContact.location);
		}else{
			for (final Contact contact : ContactList.contacts.values()) {
				CreateMarker(contact);
			}
			zoomToLoc(LocationUpdateManager.instance().locClient.getLastLocation());
		}

		for (Map.Entry<Contact, Marker> entry : markers.entrySet()) {
			final Contact contact = entry.getKey();
		    final Marker marker = entry.getValue();
			contact.location.setListener(new LocationChangedListener() {
				@Override
				public void LocationChanged(com.spotem.data.Location loc) {
					LatLng newCoords = new LatLng(loc.getLat(), loc.getLong());
					marker.setPosition(newCoords);
					if (contact == selectedContact && !isShowingOwn){
						zoomToLoc(selectedContact.location);
					}
				}
			});
		}
		
		return rootView;
	}
	
	private void InitMap(){
		SupportMapFragment mapFragment = (SupportMapFragment)getActivity().getSupportFragmentManager().findFragmentById(R.id.map);	
		map = mapFragment.getMap();
		map.setMyLocationEnabled(true);
		UiSettings mapSettings = map.getUiSettings();
		mapSettings.setCompassEnabled(false);
	}
	
	private void CreateMarker(Contact contact){
		LatLng geoCoords = new LatLng(contact.location.getLat(), contact.location.getLong());
		MarkerOptions options = new MarkerOptions();
		options.position(geoCoords);
		options.title(contact.name);
		markers.put(contact, map.addMarker(options));
	}
	
	private void zoomToLoc(com.spotem.data.Location loc){
		LatLng geoCoords = new LatLng(loc.getLat(), loc.getLong());
		zoomToLoc(geoCoords);
	}
	
	private void zoomToLoc(android.location.Location loc){
		LatLng geoCoords = new LatLng(loc.getLatitude(), loc.getLongitude());
		zoomToLoc(geoCoords);
	}
	
	private void zoomToLoc(LatLng loc){
		CameraUpdate positionUpdate = CameraUpdateFactory.newLatLngZoom(loc, 15);
		map.animateCamera(positionUpdate);
	}
	
	public static class ErrorDialogFragment extends DialogFragment {
	    private Dialog mDialog;

	    public ErrorDialogFragment() {
	        super();
	    }

	    public void setDialog(Dialog dialog) {
	        mDialog = dialog;
	    }

	    @Override
	    public Dialog onCreateDialog(Bundle savedInstanceState) {
	        return mDialog;
	    }
	}
}