<?php

include 'RestRequest.php';
include 'RestUtil.php';

class RestController {
    private $root;

    public function __construct() {
        //get root
        $dir = dirname(str_replace($_SERVER['DOCUMENT_ROOT'], '', $_SERVER['SCRIPT_FILENAME']));

        if ($dir[0] == '/')
            $dir = substr($dir, 1);

        $this->root = ($dir == '.' ? '' : $dir . '/');
    }

    public function processRequest() {

        $path = $this->getPath();
        $request = new RestRequest();
        $request->setMethod("GET");
		$request->setRequestVars($_GET);
		
        //Get the requested ressource info
        $ressourceArray = explode('/', $path);
        $request->setRessourceName($ressourceArray[0]);
		if (isset($ressourceArray[1]))
            $request->setFuncName($ressourceArray[1]);
        if (isset($ressourceArray[2]))
            $request->setRessourceID($ressourceArray[2]);
		
        $this->handleRequest($request);
    }

    private function handleRequest($request) {
        if (!$request->isValidRessourceName()){
			$this->sendResponse('501');
			return;
		}
		
        if ($this->handleCallOnController($request)) {
        } else {
			$this->sendResponse('501');
		}
    }

    private function handleCallOnController($request) {
        $controllerName = ucwords($request->getRessourceName()) . "Controller";
        $funcName = $request->getFuncName();

        $classMethods = get_class_methods($controllerName);
        if ($classMethods == NULL)
            return false;

        if (in_array($funcName, $classMethods)) {
            $controller = new $controllerName;
            $controller->$funcName($request);

            return true;
        }

        return false;
    }

    private function getPath() {
        $path = substr(preg_replace('/\?.*$/', '', $_SERVER['REQUEST_URI']), 1);
        $path = str_replace($this->root, '', $path);
        return preg_replace('/\.(\w+)$/i', '', $path);
    }

    public static function sendResponse($status = 200, $body = '', $content_type = 'text/html') {
        $status_header = 'HTTP/1.1 ' . $status . ' ' . RestUtils::getStatusCodeMessage($status);
        header($status_header);
        header('Content-type: ' . $content_type);

        if ($body != '') {
            echo $body;
            exit;
        } else {
            $message = '';

            switch ($status) {
                case 401 :
                    $message = 'You must be authorized to view this page.';
                    break;
                case 404 :
                    $message = 'The requested URL ' . $_SERVER['REQUEST_URI'] . ' was not found.';
                    break;
                case 500 :
                    $message = 'The server encountered an error processing your request.';
                    break;
                case 501 :
                    $message = 'The requested method is not implemented.';
                    break;
            }

            $signature = ($_SERVER['SERVER_SIGNATURE'] == '') ? $_SERVER['SERVER_SOFTWARE'] . ' Server at ' . $_SERVER['SERVER_NAME'] . ' Port ' . $_SERVER['SERVER_PORT'] : $_SERVER['SERVER_SIGNATURE'];

            $body = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
						<html>
							<head>
								<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
								<title>' . $status . ' ' . RestUtils::getStatusCodeMessage($status) . '</title>
							</head>
							<body>
								<h1>' . RestUtils::getStatusCodeMessage($status) . '</h1>
								<p>' . $message . '</p>
								<hr />
								<address>' . $signature . '</address>
							</body>
						</html>';

            echo $body;
            exit;
        }
    }

}

?>
