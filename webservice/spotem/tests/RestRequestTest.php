<?php

require_once dirname(__FILE__) . '/../RestRequest.php';

/**
 * Test class for RestRequest.
 * Generated by PHPUnit on 2012-05-12 at 23:31:26.
 */
class RestRequestTest extends PHPUnit_Framework_TestCase {

    /**
     * @var RestRequest
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp() {
        $this->object = new RestRequest;
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown() {
        
    }

    /**
     * @todo Implement testIsValidRessourceName().
     */
    public function testIsValidRessourceName() {
        $this->assertEquals(false, $this->object->isValidRessourceName());
        $this->object->setRessourceName("12");
        $this->assertEquals(false, $this->object->isValidRessourceName());
        $this->object->setRessourceName("!@$@");
        $this->assertEquals(false, $this->object->isValidRessourceName());
        $this->object->setRessourceName("hallo()");
        $this->assertEquals(false, $this->object->isValidRessourceName());
        $this->object->setRessourceName("hallo'");
        $this->assertEquals(false, $this->object->isValidRessourceName());
        $this->object->setRessourceName("hallo");
        $this->assertEquals(true, $this->object->isValidRessourceName());
        $this->object->setRessourceName("lei_stamm");
        $this->assertEquals(true, $this->object->isValidRessourceName());
    }

    /**
     * @todo Implement testIsValidRessourceID().
     */
    public function testIsValidRessourceID() {
        $this->assertEquals(false, $this->object->isValidRessourceID());
        $this->object->setRessourceID("wrong");
        $this->assertEquals(false, $this->object->isValidRessourceID());
        $this->object->setRessourceID("12123!");
        $this->assertEquals(false, $this->object->isValidRessourceID());
        $this->object->setRessourceID("-12");
        $this->assertEquals(false, $this->object->isValidRessourceID());
        $this->object->setRessourceID(",12");
        $this->assertEquals(false, $this->object->isValidRessourceID());
        $this->object->setRessourceID(".12");
        $this->assertEquals(false, $this->object->isValidRessourceID());
        $this->object->setRessourceID(".");
        $this->assertEquals(false, $this->object->isValidRessourceID());
        
        $this->object->setRessourceID("10");
        $this->assertEquals(true, $this->object->isValidRessourceID());
        $this->object->setRessourceID("1");
        $this->assertEquals(true, $this->object->isValidRessourceID());
        $this->object->setRessourceID("9999");
        $this->assertEquals(true, $this->object->isValidRessourceID());
        
        
    }

}

?>
