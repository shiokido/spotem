<?php
include 'Database.php';
include 'config.php';
include 'RestController.php';

spl_autoload_register();
spl_autoload_register(function($classname) {
            $classfile = __DIR__ . "/controller/{$classname}.php";

            if (file_exists($classfile)) {
                include $classfile;
            }
        });
        
spl_autoload_register(function($classname) {
            $classfile = __DIR__ . "/models/{$classname}.php";
            if (file_exists($classfile)) {
                include $classfile;
            }
        });

//header('Content-Type: application/json; charset=utf-8');
$frontController = new RESTController();
$frontController->processRequest();
?>
