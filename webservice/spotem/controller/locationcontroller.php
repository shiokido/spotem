<?php

class LocationController {
	public function set($request) {
		$memcache = new Memcache;  
		$memcache->connect('localhost', 11211) or die ("Could not connect"); 
		
		$phoneNumber = $request->getRessourceID();
		$lat = $request->getRequestVars()["lat"];
		$long = $request->getRequestVars()["long"];
		
		$loc = new Location;
		$loc->lat = $lat; 
		$loc->long = $long; 
	
		$memcache->set($phoneNumber, $loc, false, 60);
		$loc = $memcache->get($phoneNumber);
		
		echo "success";
	}

	public function get($request) {
		$memcache = new Memcache;  
		$memcache->connect('localhost', 11211) or die ("Could not connect"); 
		$loc = $memcache->get($request->getRessourceID());
		
		if ($loc){
			echo $loc->lat . " ";
			echo $loc->long;
		}else{
			RestController::sendResponse("404");
		}
	}
}
?>